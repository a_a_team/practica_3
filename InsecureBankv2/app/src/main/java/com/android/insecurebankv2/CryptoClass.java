package com.android.insecurebankv2;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.GCMParameterSpec;

import android.security.keystore.KeyGenParameterSpec;
import android.security.keystore.KeyProperties;
import android.util.Base64;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.InvalidAlgorithmParameterException;
import java.security.NoSuchProviderException;
import java.security.SignatureException;
import java.security.UnrecoverableEntryException;
import java.security.cert.CertificateException;
import java.util.Enumeration;

/*
The page that holds the logic for encryption and decryption used in the application
@author Dinesh Shetty
*/
public class CryptoClass {


	private static final String TRANSFORMATION = "AES/GCM/NoPadding";
	private static final String ANDROID_KEY_STORE = "AndroidKeyStore";
	private static final String ALIAS = "MYALIAS";

	private KeyStore keyStore;


	private byte[] encryption;
	private byte[] iv;


	public CryptoClass() throws CertificateException, NoSuchAlgorithmException, KeyStoreException,
			IOException{
		initKeyStore();
	}

	private void initKeyStore() throws KeyStoreException, CertificateException,
			NoSuchAlgorithmException, IOException {
		keyStore = KeyStore.getInstance(ANDROID_KEY_STORE);
		keyStore.load(null);
	}

	private SecretKey getSecretKey() throws NoSuchAlgorithmException,
			UnrecoverableEntryException, KeyStoreException, NoSuchProviderException, InvalidAlgorithmParameterException {
		Enumeration<String> aliases = keyStore.aliases();
		boolean createdKey = false;
		while(aliases.hasMoreElements()){
			if(ALIAS.equals(aliases.nextElement())){
				createdKey=true;
				break;
			}
		}
		if(createdKey)
			return ((KeyStore.SecretKeyEntry) keyStore.getEntry(ALIAS, null)).getSecretKey();
		else{ //no key stored -> create new one
			KeyGenerator keyGenerator = KeyGenerator
					.getInstance("AES", ANDROID_KEY_STORE);
			KeyGenParameterSpec.Builder builder = new KeyGenParameterSpec.Builder(ALIAS,
					KeyProperties.PURPOSE_ENCRYPT | KeyProperties.PURPOSE_DECRYPT);
			KeyGenParameterSpec keySpec = builder
					.setKeySize(256)
					.setBlockModes(KeyProperties.BLOCK_MODE_GCM)
					.setEncryptionPaddings(KeyProperties.ENCRYPTION_PADDING_NONE)
					.build();
			keyGenerator.init(keySpec);
			return keyGenerator.generateKey();
		}
	}

	/*
	The function that uses the aes256 decryption function
	theString: Ciphertext input to the decryption function
	plainText: Plaintext output of the encryption operation
	*/
	public String aesDeccryptedString(String theString) throws UnsupportedEncodingException, InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException, UnrecoverableEntryException, KeyStoreException, NoSuchProviderException {
		byte[] textBytes = Base64.decode(theString, Base64.DEFAULT);
		byte[] iv = new byte[12];
		byte[] ciphertext = new byte[textBytes.length-iv.length];
		System.arraycopy(textBytes,0,iv,0,iv.length);

		for(int i = iv.length;i<textBytes.length;i++){
			ciphertext[i-iv.length] = textBytes[i];
		}

		SecretKey key = getSecretKey();
		GCMParameterSpec spec = new GCMParameterSpec(128, iv);
		Cipher cipher = Cipher.getInstance(TRANSFORMATION);
		cipher.init(Cipher.DECRYPT_MODE, key, spec);
		byte[] clearText = cipher.doFinal(ciphertext);
		return new String(clearText, "UTF-8");
	}

	/*
	The function that uses the aes256 encryption function
	theString: Plaintext input to the encryption function
	cipherText: Ciphertext output of the encryption operation
	*/
	public String aesEncryptedString(String theString) throws UnrecoverableEntryException, NoSuchAlgorithmException, KeyStoreException,
			NoSuchProviderException, NoSuchPaddingException, InvalidKeyException, IOException,
			InvalidAlgorithmParameterException, SignatureException, BadPaddingException,
			IllegalBlockSizeException {
		SecretKey key = getSecretKey();
		Cipher cipher = Cipher.getInstance(TRANSFORMATION);
		cipher.init(Cipher.ENCRYPT_MODE, key);
		byte[] t = theString.getBytes("UTF-8");
		byte[] ciphertext = cipher.doFinal(t);
		byte[] iv = cipher.getIV(); //12 bytes
		byte[] text = new byte[ciphertext.length + iv.length];

		System.arraycopy(iv, 0, text, 0, iv.length);
		System.arraycopy(ciphertext, 0, text, iv.length, ciphertext.length);
		return Base64.encodeToString(text,Base64.DEFAULT);
	}
}